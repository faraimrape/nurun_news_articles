<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
    //Create a controller function for the home page
    public function index()
    {
        $title = 'Welcome to Nurun Blogs';
        return view('pages.index')->with('title', $title);
    }

    //Create a controller function for the about page
    public function about()
    {
        $aboutus = 'About Us';
        return view('pages.about')->with('aboutus', $aboutus);
    }

    //Create a controller function for the services page
    public function services()
    {
        $services = 'Our Services';
        return view('pages.services')->with('services', $services);
    }
}
