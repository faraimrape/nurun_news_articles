<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class TrashController extends Controller
{
    public function index()
    {

        $posts = Post::onlyTrashed()->get();
        return view('trash.index')->with('posts', $posts);

    }

    public function update(Request $request, $id)
    {
        // Create Post
        $post = Post::find($id);
        $post = Post::withTrashed()
            ->where('id', $id)
            ->restore();
        return redirect('/posts')->with('success', 'News article has been restored, Edit post to restore your cover image');
    }

}
