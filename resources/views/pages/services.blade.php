@extends('layouts.app')
@section('content')
<div class="jumbotron text-center">
    <h1>{{$services}}</h1>
    <p>We offer personalized news article blogging in the simplest possible way</p>
</div>
@endsection
