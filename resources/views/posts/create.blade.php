@extends('layouts.app')
@section('content')
<div class="card">
    <div class="card-header">
        <h4>Create a news post</h4>
    </div>
    <div class="card-body">
        {!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data'])
        !!}
        <div class="form-group">
            {{Form::label('title', 'Title')}}
            {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'News Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('category', 'Category/Tag')}}
            {{Form::text('category', '', ['class' => 'form-control' , 'placeholder' => 'News Category'])}}
        </div>
        <div class="form-group">
            {{Form::label('body', 'News Article Body')}}
            {{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'News
            Article Body'])}}
        </div>
        <div class="form-group">
            {{Form::file('cover_image')}}
        </div>
        {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}
    </div>
</div>
@endsection