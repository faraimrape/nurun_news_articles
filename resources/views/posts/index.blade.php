@extends('layouts.app')
@section('content')
<div class="card">
    <div class="card-header">
        <h4>News articles</h4>
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for article name">
    </div>
    <ul id="myUL">
        <div class="card-body">
            @if(count($posts) > 0)
            @foreach($posts as $post)
            <li>
                <div class="jumbotron">
                    <div class="row">
                        <div class="col-md-2 col-sm-2">
                            <img style="width:80%" src="/storage/cover_images/{{$post->cover_image}}">
                        </div>
                        <div class="col-md-10 col-sm-10">
                            <h4><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                            <h5><small>category</small> - {{$post->category}}</h5>
                            <small><b>Written on {{$post->created_at}} by {{$post->user->name}}</b></small>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
            {{$posts->links()}}
            @else
            <p>No posts found</p>
            @endif
        </div>
    </ul>
    <script>
        function myFunction() {
            // Declare variables
            var input, filter, ul, li, a, i;
            input = document.getElementById('myInput');
            filter = input.value.toUpperCase();
            ul = document.getElementById("myUL");
            li = ul.getElementsByTagName('li');

            // Loop through all list items, and hide those who don't match the search query
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("a")[0];
                if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        }
    </script>
</div>
<br />


@endsection
