@extends('layouts.app')
@section('content')
<div class="card">
    <div class="card-header">
        <h4>{{$post->title}}</h4>
        <h5><small>category - </small>{{$post->category}}</h5>
        <a href="/posts" class="btn btn-primary">Go Back</a>
    </div>
    <div class="card-body">
        <img style="width:40%" src="/storage/cover_images/{{$post->cover_image}}">
        <br><br>
        <div>
            {!!$post->body!!}
        </div>
        <hr>
        <small><b>Written on {{$post->created_at}} by {{$post->user->name}}</b></small>
        <hr>
        @if(!Auth::guest())
        @if(Auth::user()->id == $post->user_id)
        <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit Article</a>
        <br /><br />
        {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' =>
        'pull-right'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete / Disable Article', ['class' => 'btn btn-danger'])}}
        {!!Form::close()!!}
        @endif
        @endif
    </div>
</div>
@endsection
