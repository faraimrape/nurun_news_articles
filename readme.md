If you are setting up your environment for the first time download and configure the following tools
1.	Download and install (xampp-win32-7.2.11-0-VC15-installer.exe) in your C directory path
2.	Download and install composer dependency manager (Composer-Setup.exe) and point to the path of your php.exe runtime evironment within the xampp installation you just installed
3.	Download and install Visual studio Code 
4.	Download and install GIT Bash 
5.	Download and install Source tree for version control and easy repository cloning
if your  environment is already setup, skip the instructions above and proceed to clone your bitbucket remote directory through source tree or your GIT terminal at the following URL.  
(git clone https://faraimrape@bitbucket.org/faraimrape/nurun_news_articles.git)

1.	Navigate to your cloned folder directory through your cmd command line or the git terminal
2.	once there, proceed to run the the composer install command within your terminal or cmd line
3.	Once done run the following commad in your terminal as well cp .env.example .env
4.	Once done create a blank database though phpmyadmin or mysqlwork bench, then within your code editor open env file from your cloned root directory and fill in your credentials for database connection i.e
a.	 DB_CONNECTION=mysql
b.	DB_HOST=127.0.0.1
c.	DB_PORT=3306
d.	DB_DATABASE=nurun_news_site
e.	DB_USERNAME=test
f.	DB_PASSWORD=testuser)

5. Once done, in your terminal run command php artisan key:generate
6. followed by  php artisan migrate
7. followed by php artisan storage:link
8. then lastly  php artisan serve
9. Take the link generated in your terminal and open your browser, paste and run the 		application